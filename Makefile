##
## Makefile for lem_in in /home/haisso_m/epitech/2/projects/C_Prog_Elem/lem-in
## 
## Made by malek haissous
## Login   <haisso_m@epitech.net>
## 
## Started on  Tue Apr 23 21:21:33 2013 malek haissous
## Last update Tue Apr 23 21:54:21 2013 malek haissous
##

NAME	= lem_in

SRC_D	= src
INC_D	= include
BIN_D	= bin

SRC_L	=		\
	main.c		\

SRCS	= $(addprefix $(SRC_D)/,$(SRC_L))
OBJS	= $(SRCS:.c=.o)
BIN	= $(NAME)

CFLAGS	+= -I$(INC_D)

CFLAGS	+= -Wall -Wextra
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -O2

all: $(BIN)

$(BIN): $(OBJS)
	mkdir -p $(BIN_D)
	$(CC) -o $(BIN) $(OBJS)

clean:
	$(RM) -v $(OBJS)

fclean: clean
	$(RM) -v $(BIN)

re: fclean all

.PHONY: all clean fclean re
